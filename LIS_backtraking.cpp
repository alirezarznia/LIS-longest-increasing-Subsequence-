#include 	<cstdio>
#include	<iostream>
#include	<cmath>
#include 	<cstring>
#include 	<cstdlib>
#include 	<vector>
#include 	<string>
#include 	<algorithm>
#include 	<queue>
#include 	<deque>
#include 	<set>
#include 	<stack>
#include 	<map>
#include 	<sstream>
#include 	<ctime>
#include	<iomanip>
#include 	<functional>

#define 	Time 		printf("\nTime : %.3lf s.\n", clock()*1.0/CLOCKS_PER_SEC)
#define 	For(J,R,K) 	for(ll J=R;J<K;++J)
#define 	Rep(I,N) 	For(I,0,N)
#define 	MP 			make_pair
#define 	ALL(X) 		(X).begin(),(X).end()
#define 	SF 			scanf
#define 	PF 			printf
#define 	pii 		pair<long long,long long>
#define 	pdd 		pair<double , double>
#define 	Sort(v) 	sort(ALL(v))
#define 	Test 		freopen("a.in","r",stdin)
#define 	Testout 	freopen("a.out","w",stdout)
#define 	pb 			push_back
#define 	Set(a,n) 	memset(a,n,sizeof(a))
#define 	MAXN 		100000+99
#define 	EPS 		1e-15
#define 	inf 		1ll<<62

typedef long long ll;

using namespace std;
vector<ll>r;
ll k;
ll dp[102][102];
ll BACK(ll num , ll e , ll cur)
{
    if(dp[cur][e] !=-1)
        return dp[cur][e];
    if(e==k)
        return 1;
    if(num== 0)
        return 0;
    if(r[num-1] < r[cur])
    {
        return dp[cur][e] = BACK(num-1 , e , cur) + BACK(num-1 , e + 1 , num-1);
    }
    else
        return dp[cur][e]= BACK(num-1 , e, cur);
}
int main()
{
    //Test;
    ll n ;
    while(cin>>n>>k)
    {
        Set(dp ,-1);

        if(n==0 && k==0)
            break;
        Rep(i ,n)
        {

            ll x;
            cin>>x;
            r.push_back(x);
        }
        r.push_back(10002);
        cout<<BACK(r.size()-1 , 0 , r.size()-1)<<endl;
        r.clear();
    }
    return 0;
}
